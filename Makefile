PROJECT_NAME=island-counter

build:
	docker build -t island-counter .

run: build
	docker run --rm $(PROJECT_NAME) python3 main.py -f $(FILEPATH)

test: build
	docker run --rm $(PROJECT_NAME) pytest src/tests.py
