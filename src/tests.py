import numpy as np
import pytest

from src.exceptions import (
    EmptyMatrixException,
    MatrixShapeException,
    MatrixValuesException,
)
from src.file_parser import file_parser
from src.island_counter import island_counter


def random_matrix(n: int, m: int) -> np.ndarray:
    return np.random.choice([0, 1], size=(n, m), p=[0.75, 0.25])


def test_empty_file():
    with pytest.raises(EmptyMatrixException):
        file_parser.read("/app/matrices/matrix")


def test_zero_dim_file():
    matrix = file_parser.read("/app/matrices/matrix2")
    result = island_counter.compute(matrix)
    assert result == 1


def test_one_dim_row_file():
    matrix = file_parser.read("/app/matrices/matrix3")
    result = island_counter.compute(matrix)
    assert result == 3


def test_one_dim_column_file():
    matrix = file_parser.read("/app/matrices/matrix4")
    result = island_counter.compute(matrix)
    assert result == 2


def test_zero_row_matrix():
    matrix = file_parser.read("/app/matrices/matrix5")
    result = island_counter.compute(matrix)
    assert result == 3


def test_diagonal_matrix():
    matrix = file_parser.read("/app/matrices/matrix6")
    result = island_counter.compute(matrix)
    assert result == 1


def test_square_diagonal_matrix():
    matrix = file_parser.read("/app/matrices/matrix7")
    result = island_counter.compute(matrix)
    assert result == 1


def test_upper_diagonal_matrix():
    matrix = file_parser.read("/app/matrices/matrix8")
    result = island_counter.compute(matrix)
    assert result == 1


def test_lower_diagonal_matrix():
    matrix = file_parser.read("/app/matrices/matrix9")
    result = island_counter.compute(matrix)
    assert result == 1


def test_lower_diagonal_matrix_with_hole():
    matrix = file_parser.read("/app/matrices/matrix10")
    result = island_counter.compute(matrix)
    assert result == 2


def test_lambda_diagonal_matrix():
    matrix = file_parser.read("/app/matrices/matrix11")
    result = island_counter.compute(matrix)
    assert result == 1


def test_wrong_symbol():
    with pytest.raises(MatrixValuesException):
        file_parser.read("/app/matrices/matrix12")


def test_file_error():
    with pytest.raises(MatrixShapeException):
        file_parser.read("/app/matrices/matrix13")


def test_matrix_14():
    matrix = file_parser.read("/app/matrices/matrix14")
    result = island_counter.compute(matrix)
    assert result == 1


def test_matrix_15():
    matrix = file_parser.read("/app/matrices/matrix15")
    result = island_counter.compute(matrix)
    assert result == 1


def test_matrix_16():
    matrix = file_parser.read("/app/matrices/matrix16")
    result = island_counter.compute(matrix)
    assert result == 0


def test_matrix_17():
    matrix = file_parser.read("/app/matrices/matrix17.txt")
    result = island_counter.compute(matrix)
    assert result == 3


def test_big_random_matrix():
    matrix = file_parser.read("/app/matrices/matrixA")
    result = island_counter.compute(matrix)
    assert result


def test_biger_random_matrix():
    matrix = random_matrix(10**4, 10**4)
    result = island_counter.compute(matrix)
    assert result


def test_ones_matrix():
    matrix = np.ones((10**3, 10**3))
    result = island_counter.compute(matrix)
    assert result == 1
