class MatrixException(Exception):
    pass


class MatrixValuesException(MatrixException):
    def __init__(self):
        super().__init__("Provided matrix should contain only value: 0 or 1")


class EmptyMatrixException(MatrixException):
    def __init__(self):
        super().__init__("File is empty")


class MatrixShapeException(MatrixException):
    def __init__(self):
        super().__init__("Matrix has inconsistent shape")


class FilePathException(Exception):
    def __init__(self):
        super().__init__("Wrong file path parameter")
