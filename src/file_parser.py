import numpy as np

from src.exceptions import (
    EmptyMatrixException,
    MatrixShapeException,
    MatrixValuesException,
)


class FileParser:
    _DELIMITER = 1
    _DATA_TYPE = "int8"

    def read(self, path: str) -> np.ndarray:
        try:
            matrix = np.genfromtxt(
                path,
                delimiter=self._DELIMITER,
                dtype=self._DATA_TYPE,
                invalid_raise=True,
            )
            assert np.array_equal(matrix, matrix.astype(bool))
            if matrix.shape == (0,):
                raise EmptyMatrixException()
        except AssertionError:
            raise MatrixValuesException()
        except ValueError:
            raise MatrixShapeException()
        return matrix


file_parser = FileParser()
