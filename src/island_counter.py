from collections import deque
from typing import Deque, Iterator, List, Tuple

import numpy as np


class MatrixDims:
    def __init__(self, row_number, col_number, dimensions):
        self.rows = row_number
        self.cols = col_number
        self.dim = dimensions


class IslandCounter:
    _MATRIX_TRANSITIONS: List[Tuple[int, int]] = [
        (1, 0),
        (1, 1),
        (0, 1),
        (-1, 1),
        (-1, 0),
        (-1, -1),
        (0, -1),
        (1, -1),
    ]
    _LIST_TRANSITIONS: List[int] = [-1, 1]
    _MARK_SIGN: int = 4

    def compute(self, matrix: np.ndarray) -> int:
        matrix_dims = self._get_matrix_dims(matrix)
        islands = 0

        if matrix_dims.dim == 2:
            for row in range(matrix_dims.rows):
                for col in range(matrix_dims.cols):
                    if matrix[row][col] == 1:
                        islands += 1
                        for new_row, new_col in self._land_search(
                            row, col, matrix_dims
                        ):
                            if matrix[new_row][new_col] == 1:
                                self._bfs(new_row, new_col, matrix, matrix_dims)

        elif matrix_dims.dim == 1:
            for row in range(matrix_dims.rows):
                if matrix[row] == 1:
                    islands += 1
                    for new_row in self._flat_land_search(row, matrix_dims):
                        if matrix[new_row] == 1:
                            self._flat_bfs(new_row, matrix, matrix_dims)

        elif matrix_dims.dim == 0:
            return int(matrix)
        return islands

    def _bfs(self, row: int, col: int, matrix: np.ndarray, matrix_dims: MatrixDims):
        que: Deque = deque()
        que.append((row, col))
        matrix[row][col] = self._MARK_SIGN

        while que:
            row, col = que.popleft()

            for new_row, new_col in self._land_search(row, col, matrix_dims):
                if matrix[new_row][new_col] == 1:
                    matrix[new_row][new_col] = self._MARK_SIGN
                    que.append((new_row, new_col))

    def _flat_bfs(self, row: int, matrix: np.ndarray, matrix_dims: MatrixDims):
        que: Deque = deque()
        que.append(row)
        matrix[row] = self._MARK_SIGN

        while que:
            row = que.popleft()

            for new_row in self._flat_land_search(row, matrix_dims):
                if matrix[new_row] == 1:
                    matrix[new_row] = self._MARK_SIGN
                    que.append(new_row)

    def _flat_land_search(self, row: int, matrix_dims: MatrixDims) -> Iterator[int]:
        for horizontal in self._LIST_TRANSITIONS:
            new_row = row + horizontal
            if 0 <= new_row < matrix_dims.rows:
                yield new_row

    def _get_matrix_dims(self, matrix: np.ndarray) -> MatrixDims:
        shape = matrix.shape
        if matrix.ndim == 2:
            return MatrixDims(row_number=shape[0], col_number=shape[1], dimensions=2)
        elif matrix.ndim == 0:
            return MatrixDims(row_number=0, col_number=0, dimensions=0)
        elif matrix.ndim == 1:
            return MatrixDims(row_number=shape[0], col_number=0, dimensions=1)
        raise NotImplementedError

    def _land_search(
        self, row: int, col: int, matrix_dims: MatrixDims
    ) -> Iterator[Tuple[int, int]]:
        for horizontal, vertical in self._MATRIX_TRANSITIONS:
            new_row, new_col = row + horizontal, col + vertical
            if 0 <= new_row < matrix_dims.rows and 0 <= new_col < matrix_dims.cols:
                yield new_row, new_col


island_counter = IslandCounter()
