# Island Counter

## Problem

At the beginning of this task, you are given a path to the file that denotes a
two-dimensional array which visualizes a map. Each element of this array will take
one of the following values:
0 (which represents water),
1 (which represents land).
These values will form islands on water.

For example:
```angular2html
0 0 0 0 0 0 0 0 0
0 1 0 0 0 0 0 0 0
1 1 1 0 0 0 1 0 0
1 1 0 0 0 1 1 1 0
0 0 0 0 0 1 1 0 0
0 0 1 0 0 0 0 0 0
1 1 0 0 0 0 0 0 0
0 0 0 0 0 1 1 0 0
```

This example illustrates 4 islands. Counting from left upper corner:

● First Island is made of six land elements,

● Second Island is also made of six land elements.

● Third island consists of 3 elements,

● The last one is made of two elements.


The invocation of your bash script will look like:
./your_script.sh <path_to_the_file>
where the file denoted by the path will be a text file with only zeros (ASCII character 48) and ones (ASCII character 49) and end-of-line, i.e.:
```angular2html
000000000
010000000
111000100
110001110
000001100
001000000
110000000
000001100
```

Write a short program, which will count the number of islands. The following data
structure could be varying in size. The only output of the program written to STDOUT
should be the number (all additional diagnostics information, if needed, should be
written to STDERR).

Take into the assumption, that data structure given to you could be very large in size,
so beware of implementation that could result in stack overflow or out of memory
exception.
Technical restrictions:

● Please use a “reasonable” number of dependencies on external libraries (the
best approach is to use only the unit testing framework)

● For java solution: please use Java 11+ and gradle 5+ or maven 3+

● For python solution: please use python3+ and pip20+


### Run program:
```shell
./run_app.sh matrices/matrix17.txt
or
make run FILEPATH=matrices/matrix17.txt
```

### Run tests:
```python
make test
```

### Note:
Please choose files from `app/` directory for `docker` to add in volumes.
