#!/bin/sh

if [ "$1" = '' ]; then
  echo "Path to matrix file is required."
  exit 1
fi

result=$(make run FILEPATH=$1 2>&1 )
if [ "$?" = 2 ]; then
  echo "${result}" | tail -2
  exit 2
fi

echo "${result}" | tail -1

