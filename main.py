import argparse
import os
import sys

from src.exceptions import FilePathException
from src.file_parser import file_parser
from src.island_counter import island_counter

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f", "--filepath", required=True, type=str, help="Path to matrix file"
    )
    args = parser.parse_args()
    path = args.filepath
    if not os.path.exists(os.path.dirname(path)):
        raise FilePathException()

    matrix = file_parser.read(path)
    result = island_counter.compute(matrix)
    sys.stdout.write(str(result))
